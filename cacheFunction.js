function cacheFunction() {

    let obj = {
        name:'Tony',
        age:'54',
        work:'superhero'
    }
        
    function cb(){
        console.log(obj.name + ' is a ' + obj.age + 'years old ' + obj.work);
    }

    return cb()
}
cacheFunction() 